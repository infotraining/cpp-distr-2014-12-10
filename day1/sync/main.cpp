#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>

using namespace std;

volatile long counter = 0;
volatile long counter_lf = 0;
atomic<long> counter_atom{0};

mutex mtx;

class spin_lock
{
    atomic_flag flag;
public:
    spin_lock() : flag(ATOMIC_FLAG_INIT) {}
    void lock()
    {
        while(flag.test_and_set())
        {
            this_thread::yield();
        }
    }

    void unlock()
    {
        flag.clear();
    }

};

void increase()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        mtx.lock();
        ++counter;
        mtx.unlock();
    }
}

void increase_without_mtx()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        ++counter_lf;
    }
}

void increase_atomic()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        counter_atom.fetch_add(1, memory_order_seq_cst);
    }
}

spin_lock slock;
long counter_spin = 0;

void increase_atomic_spinlock()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        slock.lock();
        ++counter_spin;
        slock.unlock();
    }
}

int main()
{
    vector<thread> thds;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(increase);
    for(auto& th :thds) th.join();

    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us";

    cout << " Counter = " << counter << endl;
    thds.clear();
    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(increase_without_mtx);
    for(auto& th :thds) th.join();

    end = chrono::high_resolution_clock::now();

    cout << "Elapsed " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us";

    cout << " Counter_lf = " << counter_lf << endl;

    thds.clear();
    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(increase_atomic);
    for(auto& th :thds) th.join();

    end = chrono::high_resolution_clock::now();

    cout << "Elapsed " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us";

    cout << " Counter_atom = " << counter_atom << endl;

    thds.clear();
    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(increase_atomic_spinlock);
    for(auto& th :thds) th.join();

    end = chrono::high_resolution_clock::now();

    cout << "Elapsed " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us";

    cout << " Counter_spin = " << counter_spin << endl;

    return 0;
}

