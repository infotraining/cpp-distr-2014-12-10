#include <random>
#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <algorithm>

using namespace std;

void calc_counts(long N, long& counter)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0, 1);

    double x, y;
    long cnt{};

    for (int n = 0; n < N; ++n) {
        x = dis(gen);
        y = dis(gen);
        if (x*x + y*y <1)
            ++cnt;
    }
    counter += cnt;
}

int main()
{
    int n_of_threads = max(thread::hardware_concurrency(), 1u);

    const long N = 100000000;


    vector<thread> threads;

    for (int i = 1 ; i < 256 ; i = i * 2)
    {
        n_of_threads = i;
        vector<long> counts(n_of_threads);
        //long counts{0}; // race condition
        auto start = chrono::high_resolution_clock::now();

        for(int i = 0 ; i < n_of_threads ; ++i)
            threads.emplace_back(&calc_counts, N/n_of_threads, ref(counts[i]));

        for(auto& th : threads) th.join();
        threads.clear();

        auto end = chrono::high_resolution_clock::now();

        cout << "Elapsed " << chrono::duration_cast<chrono::milliseconds>(end-start).count();
        cout << " ms";
        cout << " Threads = " << n_of_threads;
        cout << " Pi = " << accumulate(counts.begin(), counts.end(), 0.0)/(double)N * 4 << endl;
        //cout << " Pi = " << double(counts)/(double)N * 4 << endl;
    }
}
