#include <iostream>
#include <thread>

using namespace std;

struct Loud
{
    Loud() { cout << "Constructor" << endl; }
    ~Loud() { cout << "Destructor" << endl; }
};

void hello()
{
    Loud l;
    cout << "Hello world by thread " << this_thread::get_id() << endl;
    this_thread::sleep_for(chrono::seconds(1));
}

void hello2(int id)
{
    cout << "Hello from " << id << endl;
}

void result(int& ans)
{
    ans = 42;
}

class Task
{
public:
    void operator ()(int id)
    {
        cout << "Hello from functor " << id << endl;
    }
};

int main()
{
    cout << "Hello thread!" << endl;
    thread th1(&hello);
    thread th2(&hello2, 10);
    int ans{};
    thread th3(&result, ref(ans));
    thread th4(Task(), 11);
    thread th5( [] { cout << "Hello from lambda" << endl;});
    cout << "After launching the thread" << endl;
    th1.join();
    th2.join();
    th3.join();
    th4.join();
    th5.join();
    cout << "Ans = " << ans << endl;
    cout << "After join" << endl;
    return 0;
}

