#include <iostream>
#include <thread>
#include <vector>

using namespace std;

struct Loud
{
    Loud() { cout << "Constructor" << endl; }
    ~Loud() { cout << "Destructor" << endl; }
};

void hello()
{
    //Loud l;
    this_thread::sleep_for(chrono::seconds(1));
    cout << "Hello world by thread " << this_thread::get_id() << endl;
    this_thread::sleep_for(chrono::seconds(1));
}

thread return_thread()
{
    thread th(hello);
    return th;
}

int main()
{
    thread th1{};
    thread th2{hello};
    th1 = move(th2);
    thread th3(return_thread());
    cout << "Is 1 joinable? " << th1.joinable() << endl;
    cout << "Is 2 joinable? " << th2.joinable() << endl;
    th1.join();
    th3.join();

    vector<thread> threads;
    for (int i = 0 ; i < 10 ; ++i)
    {
        //threads.push_back(thread([i] { cout << "Hello by " << i << endl;}));
        threads.emplace_back([i] { cout << "Hello by " << i << endl;});
    }
    threads.push_back(return_thread());

    for (auto& th : threads) th.join();
    return 0;
}
















