#include <iostream>
#include <functional>
#include <thread>
#include <vector>
#include <future>
#include "../TSQ/thread_safe_queue.h"

using namespace std;

//template<typename T>
//using task_t = function<void(T)>;
using task_t = function<void(void)>;

class thread_pool
{
    //thread_safe_queue<task_t<void>> q;
    thread_safe_queue<task_t> q;
    vector<thread> workers;
public:
    thread_pool(int pool_size)
    {
        for (int i = 0 ; i < pool_size ; ++i)
            workers.emplace_back( [this]
                {
                    for(;;)
                    {
                        task_t task;
                        q.pop(task);
                        if (!task)
                        {
                            q.push(task);
                            return;
                        }
                        task();
                    }
                }
            );
    }

    template<typename F>
    auto async(F fun) -> future<decltype(fun())>
    {
        using res_t = decltype(fun());
        auto task = make_shared<packaged_task<res_t()>>(fun);
        future<res_t> resf = task->get_future();
        q.push([task] { (*task)(); });
        return resf;
    }

    void send(task_t task)
    {
        if (task)
            q.push(task);
    }

    ~thread_pool()
    {
        q.push(task_t());
        for(auto& w: workers) w.join();
    }
};

void hello()
{
    cout << "Hello from function" << endl;
}

int main()
{
    thread_pool tp(4);
    tp.send( [] { cout << "Hello from lambda"; } );

    future<int> res = tp.async( [] { return 42; });

    cout << "Ans = " << res.get() << endl;

    for (int i = 1 ; i <= 20 ; ++i)
    {
        tp.send( [i] {
                         cout << "Lambda id " << i << endl;
                         this_thread::sleep_for(chrono::milliseconds(100*i));
                     }
               );
    }
    return 0;
}

