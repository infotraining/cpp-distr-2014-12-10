#include <iostream>
#include <thread>
#include <mutex>
#include <vector>

using namespace std;
mutex mtx;

void worker_not_good() // unguarded mutexes
{
    cout << "Worker has started " << this_thread::get_id() << endl;
    bool has_lock = mtx.try_lock();
    while (!has_lock)
    {
        cout << "Waiting for lock " << this_thread::get_id() << endl;
        this_thread::sleep_for(100ms);
        has_lock = mtx.try_lock();
    }
    cout << "I have the lock " << this_thread::get_id() << endl;
    this_thread::sleep_for(1s);
    mtx.unlock();
}

void worker() // unique lock
{
    cout << "Worker has started " << this_thread::get_id() << endl;
    unique_lock<mutex> ul(mtx, try_to_lock);
    while (!ul.owns_lock())
    {
        cout << "Waiting for lock " << this_thread::get_id() << endl;
        this_thread::sleep_for(100ms);
        ul.try_lock();
    }
    cout << "I have the lock " << this_thread::get_id() << endl;
    this_thread::sleep_for(1s);
}


int main()
{
    vector<thread> thds;
    thds.emplace_back(worker);
    thds.emplace_back(worker);
    for (auto& th : thds) th.join();

    return 0;
}


