#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for(int i = 0 ; i < 20 ; ++i)
    {
        {
            lock_guard<mutex> lk(mtx);
            q.push(i);
            cond.notify_one();
            cout << "just produced " << i << endl;
        }
        this_thread::sleep_for(200ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> lk(mtx);
//        while (q.empty())
//        {
//            cond.wait(lk);
//        }
        cond.wait(lk, [] { return !q.empty(); });
        cout << id << " just got " << q.front() << endl;
        q.pop();
    }
}

int main()
{
    thread th(producer);
    thread con1(consumer, 1);
    thread con2(consumer, 2);
    th.join();
    con1.join();
    con2.join();
    return 0;
}

