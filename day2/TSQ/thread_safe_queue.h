#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <mutex>
#include <queue>
#include <condition_variable>

template<typename T>
class thread_safe_queue
{
    std::mutex mtx;
    std::condition_variable cond;
    std::queue<T> q;
public:
    thread_safe_queue() {}

    void push(const T& item)
    {
        std::lock_guard<std::mutex> l(mtx);
        q.push(item);
        cond.notify_one();
    }

    void push(T&& item)
    {
        std::lock_guard<std::mutex> l(mtx);
        q.push(std::move(item));
        cond.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> l(mtx);
        cond.wait(l, [this] {return !q.empty(); });
        item = std::move(q.front());
        q.pop();
    }
};

#endif // THREAD_SAFE_QUEUE_H
