#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>
#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for(int i = 0 ; i < 20 ; ++i)
    {
        {
            q.push(i);
            cout << "just produced " << i << endl;
        }
        this_thread::sleep_for(200ms);
    }
    q.push(-1);
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg);
        if (msg == -1)
        {
            q.push(-1);
            return;
        }
        cout << id << " just got " << msg << endl;
    }
}

int main()
{
    thread th(producer);
    thread con1(consumer, 1);
    thread con2(consumer, 2);
    th.join();
    con1.join();
    con2.join();
    return 0;
}

