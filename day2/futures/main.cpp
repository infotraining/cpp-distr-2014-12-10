#include <iostream>
#include <future>
#include <vector>
#include <thread>
#include <stdexcept>

using namespace std;

int answer(int id)
{
    this_thread::sleep_for(chrono::milliseconds(500));
    if (id == 13) throw logic_error("Unlucky number");
    return 42*id;
}

void get_answer(future<int>&& fut)
{
    cout << "Ans = " << fut.get() << endl;
}

int main()
{
    cout << "Futures" << endl;

    // without thread
    get_answer(async(launch::deferred, answer, 10));

    // using thread
    // simple way = future<int> res = async(launch::async, answer, 13);
    packaged_task<int(int)> pt(answer);
    /*
    {
        class packaged_task
        {
            promise p;
            packaged_task(f, ...)
            operator ()(...)
            {
                try
                p.set_value(f(...));
                catch
                    exc_ptr = current_exception()
            }
            f<T> get_future()
            {
                p.get_future();
            }
        };
    }*/
    future<int> res = pt.get_future();
    thread th(move(pt), 13);
    th.detach();

    cout << "working..." << endl;

    res.wait(); // blocking until thread is finished

    try
    {
        cout << "Answer = " << res.get() << endl;
    }
    catch(const logic_error& err)
    {
        cerr << "Error " << err.what() << endl;
    }

    return 0;
}

