#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>
#include <stdexcept>

using namespace std;

long counter;
mutex mtx;

void increase()
{
    try
    {
        for (int i = 0 ; i < 10000 ; ++i)
        {
            lock_guard<mutex> lk(mtx);
            ++counter;
            if (counter == 100) throw logic_error("Logic error");
        }
    }
    catch(const logic_error& err)
    {
        cerr << "Error! " << err.what() << endl;
    }
}

int main()
{
    vector<thread> thds;
    try
    {
        thds.emplace_back(increase);
        thds.emplace_back(increase);
        thds.emplace_back(increase);
        for (auto& th : thds) th.join();
    }
    catch(...)
    {
        cerr << "Error!!" << endl;
    }

    cout << "Counter = " << counter << endl;
    return 0;
}

