#include <iostream>
#include <zhelpers.hpp>
#include <zmq.hpp>
#include <thread>
using namespace std;

// SERVER

int main()
{
    cout << "ZMQ fwd_sink starting" << endl;
    zmq::context_t context(1);
    zmq::socket_t sink(context, ZMQ_PULL);
    sink.bind("tcp://37.139.9.185:8887");
    zmq::socket_t fwd(context, ZMQ_PUSH);
    fwd.bind("tcp://37.139.9.185:8890");

    for(;;)
    {
        string temp = s_recv(sink);
        //cout << "got line: " << temp;
        s_send(fwd, temp);
    }
    return 0;
}
