#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>

using namespace std;

int main()
{
    cout << "Hello zmq client" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);

    socket.connect("tcp://37.139.9.185:6666");

    for(int i = 0 ; i < 10 ; ++i)
    {
        s_send(socket, "msg to server - from Leszek");
        cout << "just got " << s_recv(socket) << endl;
    }
    return 0;
}

