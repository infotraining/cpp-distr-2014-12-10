#include <iostream>
#include <zhelpers.hpp>
#include <zmq.hpp>
#include "smallpt_lib.hpp"
#include <string>
#include <thread>
#include <vector>
#include <stdlib.h>

using namespace std;

int worker ()
{
    // scheme:
    // Leszek: 123 :: rgb rgb

    zmq::context_t context(1);

    zmq::socket_t recieve(context, ZMQ_PULL);
    recieve.connect("tcp://37.139.9.185:8888");

    zmq::socket_t sink(context, ZMQ_PUSH);
    sink.connect("tcp://37.139.9.185:8887");

    while(true)
    {
        auto line_str = s_recv(recieve);
        std::cout << "line number: " << line_str << std::endl;
        istringstream iss(line_str);
        int line_no;
        int quality;
        iss >> line_no >> quality;
        auto line = scan_line(line_no, quality);
        string output;
        output = string("L3szek ") + to_string(line_no) + " :: " + line;
        s_send(sink, output);
    }
}

int main()
{
    vector<thread> thds;
    for (int i = 0 ; i < thread::hardware_concurrency() ; ++i)
    {
        thds.emplace_back(worker);
    }
    for (auto& th : thds) th.join();
}


