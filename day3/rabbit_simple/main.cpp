#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string ex_name = "Nokia-test";
string q_name = "Nokia-q";

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create("37.139.9.185", 5672,
                              "admin", "tymczasowe");
    channel->DeclareExchange(ex_name, Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareQueue(q_name, false, true, false, false);
    channel->BindQueue(q_name, ex_name, "chat");
}

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create("37.139.9.185", 5672,
                              "admin", "tymczasowe");
    channel->BasicPublish(ex_name, "chat",
                          BasicMessage::Create("[Leszek] medium is the message"));
}

void receive()
{
    Channel::ptr_t channel;
    channel = Channel::Create("37.139.9.185", 5672,
                              "admin", "tymczasowe");
    channel->BasicConsume(q_name);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    cout << "Simple AMQP demo" << endl;
    //setup();
    send();
    receive();
    return 0;
}

