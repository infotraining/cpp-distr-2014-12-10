#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <string>
#include <thread>

using namespace std;

void send()
{
    zmq::context_t con(1);
    zmq::socket_t push(con, ZMQ_PUSH);
    push.connect("tcp://37.139.9.185:8666");

    for(;;)
    {
        string msg;
        cout << ">>> ";
        getline(cin, msg);
        msg = "[Leszek] " + msg;
        s_send(push, msg);
    }
}

void subscribe()
{
    zmq::context_t con(1);
    zmq::socket_t sub(con, ZMQ_SUB);
    sub.connect("tcp://37.139.9.185:8667");
    sub.setsockopt(ZMQ_SUBSCRIBE, 0, 0);

    for(;;)
    {
        cout << s_recv(sub) << endl;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    thread ths(send);
    thread thsb(subscribe);
    ths.join();
    thsb.join();
    return 0;
}

