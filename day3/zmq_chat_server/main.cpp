#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <string>

using namespace std;

// server IP = 37.139.9.185
// on ZMQ_SUB socket you have to subscribe:
// subscriber.setsockopt(ZMQ_SUBSCRIBE, 0, 0);

int main()
{
    cout << "ZMQ chat server starting" << endl;
    zmq::context_t context(1);

    // socket for getting messages
    zmq::socket_t pull(context, ZMQ_PULL);
    pull.bind("tcp://*:8666");

    // socket for publishing messages
    zmq::socket_t pub(context, ZMQ_PUB);
    pub.bind("tcp://*:8667");

    for(;;)
    {
        string msg = s_recv(pull);
        cout << msg << endl;
        s_send(pub, msg);
    }

    return 0;
}

