#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <string>
#include <thread>

using namespace std;

int main()
{
    cout << "Render server" << endl;
    zmq::context_t con(1);
    zmq::socket_t push(con, ZMQ_PUSH);
    push.bind("tcp://*:8888");

    for(int i = 0 ; i < 768 ; ++i)
    {
        string msg = to_string(i);
        msg += " 64";
        s_send(push, msg);
        cout << "Just produced " << msg << endl;
        this_thread::sleep_for(chrono::milliseconds(200));
    }

    return 0;
}

